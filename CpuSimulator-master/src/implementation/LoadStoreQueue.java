/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import utilitytypes.EnumOpcode;
import utilitytypes.ICpuCore;
import utilitytypes.IGlobals;
import utilitytypes.IModule;
import utilitytypes.IPipeReg;
import utilitytypes.IProperties;
import utilitytypes.IRegFile;
import utilitytypes.Logger;
import utilitytypes.Operand;

/**
 *
 * @author millerti
 */
public class LoadStoreQueue extends PipelineStageBase {
    
    public LoadStoreQueue(IModule parent) {
        super(parent, "LoadStoreQueue");
        
        // Force PipelineStageBase to use the zero-operand compute() method.
        this.disableTwoInputCompute();
    }
    
    
    // Data structures
    
    public void compute() {        
        // First, mark any retired STORE instructions
        
        // Check CPU run state
        
        // If the LSQ is full and a memory instruction wants to come in from
        // Decode, optionally see if you can proactively free up a retired STORE.
        
        // See if there's room to storea an instruction received from Decode.
        // Read the input latch.  If it's valid, put the instruction into the 
        // LAST slot.
        // LSQ entries are stored in order from oldest to newest.          

        // Loop over the forwarding sources, capturing all values available
        // right now and storing them into LSQ entries needing those
        // register values.
        
        
        // For diagnostic purposes, iterate existing entries of the LSQ and add 
        // their current state to the activity list
        
        // Next, try to do things that require writing to the
        // next pipeline stage.  If we have already written the output, or
        // the output can't accept work, might as well bail out.
       /* if (wrote_output || !outputCanAcceptWork(0)) {
            setActivity(...);
            return;
        }*/	int i=0;
        Latch input = this.readInput(0).duplicate();
        InstructionBase ins = input.getInstruction();
        if (ins.isNull()) {
                return;
        }
        String instruct="";
        for( i=0;i<256;i++) {
        	if(GlobalData.Queue[i].equals("NULL") && !ins.isNull()) {
        		GlobalData.Queue[i]=input.getInstruction().toString();
        		
        	//	System.out.println("IssueQueue "+i+":"+GlobalData.Queue[i]);
        		GlobalData.mylatch[i]=input; 
        		
        		input.consume();
        		break;
        	}
        }
        
        
        //   ins=ins.duplicate();
        // Default to no squashing.

        	/*for(i=0;i<256;i++) {
        		if(GlobalData.Queue[i]!="NULL")
        		System.out.println("IssueQueue "+i+":"+GlobalData.Queue[i]);
        	}
        	*/
      
      /*  if(GlobalData.instruction !="NULL")
        setActivity(GlobalData.instruction);
*/			for(int j=0;j<256;j++) {
		if(GlobalData.Queue[j]!="NULL") {
			instruct=instruct+GlobalData.Queue[j];
			setActivity(instruct);
			instruct=instruct+"\n";
			 getCore().incIssued();
		ins=GlobalData.mylatch[j].getInstruction();
			//	ins.setInstructionString(GlobalData.Queue[j]);
			
			// GlobalData.instruction= GlobalData.instruction+"\n"+GlobalData.Queue[j];
        IGlobals globals = (GlobalData) getCore().getGlobals();
      

      

        EnumOpcode opcode = ins.getOpcode();
        Operand oper0 = ins.getOper0();
        IRegFile regfile = globals.getRegisterFile();
        
        
        forwardingSearch(GlobalData.mylatch[j]);
       

        
        
      //  input = input.duplicate();
       
        //  ins=ins.duplicate();
          boolean stall=false;
         
          Operand src1 = ins.getSrc1();
          Operand src2 = ins.getSrc2();
       // doPostedForwarding(input);
     
          
          
       Latch output;
      int output_num = lookupOutput("LSQToDCache1");
       output= this.newOutput(output_num);
       /*int output_num;
        if (opcode == EnumOpcode.MUL) {
            output_num = lookupOutput("IssueQueueToIntMUL");
            output = this.newOutput(output_num);
        } else {
            if (opcode == EnumOpcode.DIV) {
                output_num = lookupOutput("IssueQueueToIntDiv");
                output = this.newOutput(output_num);
            } else {
                if (opcode == EnumOpcode.FDIV) {
                    output_num = lookupOutput("IssueQueueToFloatDiv");
                    output = this.newOutput(output_num);
                } else {
                    if (opcode == EnumOpcode.FMUL) {
                        output_num = lookupOutput("IssueQueueToFloatMul")	;
                        output = this.newOutput(output_num);
                    } else {
                        if (opcode == EnumOpcode.FCMP || opcode == EnumOpcode.FADD || opcode == EnumOpcode.FSUB) {
                            output_num = lookupOutput("IssueQueueToFloatAddSub");
                            output = this.newOutput(output_num);
                        } else {
        if (opcode.accessesMemory()) {
                                output_num = lookupOutput("IssueQueueToMemUnit");
            output = this.newOutput(output_num);
        } else {
                                output_num = lookupOutput("IssueQueueToExecute");
            output = this.newOutput(output_num);
        }
                        }
                    }
                }
            }
        }
        */
        if(!output.canAcceptWork())
        {
        	continue;
        }
        
        
        int[] srcRegs = new int[3];
        // Only want to forward to oper0 if it's a source.
        srcRegs[0] = opcode.oper0IsSource() ? oper0.getRegisterNumber() : -1;
        srcRegs[1] = src1.getRegisterNumber();
        srcRegs[2] = src2.getRegisterNumber();
        Operand[] operArray = {oper0, src1, src2};
        
        // Loop over source operands, looking to see if any can be
        // forwarded to the next stage.
        for (int sn=0; sn<3; sn++) {
            int srcRegNum = srcRegs[sn];
            // Skip any operands that are not register sources
            if (srcRegNum < 0) {
                continue;
            }
            // Skip any that already have values
            if (operArray[sn].hasValue()) {
            	
                continue;
            }
            
            String propname = "forward" + sn;
            if (!GlobalData.mylatch[j].hasProperty(propname)) {
                // If any source operand is not available
                // now or on the next cycle, then stall.
                //Logger.out.println("Stall because no " + propname);
               // this.setResourceWait(operArray[sn].getRegisterName());
                // Nothing else to do.  Bail out.
               stall=true;
            	break;
            }
        }
        
    //    if (ins.getOpcode() == EnumOpcode.HALT) shutting_down = true;            
        
        if(stall) {
        	
        	stall=false;
        	continue;
        }
     /*   if (CpuSimulator.printForwarding) {
            for (int sn=0; sn<3; sn++) {
                String propname = "forward" + sn;
                if (input.hasProperty(propname)) {
                    String operName = PipelineStageBase.operNames[sn];
                    String srcFoundIn = input.getPropertyString(propname);
                    String srcRegName = operArray[sn].getRegisterName();
                    Logger.out.printf("# Posting forward %s from %s to %s next stage\n", 
                            srcRegName,
                            srcFoundIn, operName);
                }
            }
        }*/
                
        // If we managed to find all source operands, mark the destination
        // register invalid then finish putting data into the output latch 
        // and send it.
        
        // Mark the destination register invalid
        if (opcode.needsWriteback()) {
            int oper0reg = oper0.getRegisterNumber();
            regfile.markInvalid(oper0reg);
        }            
        
        output.copyAllPropertiesFrom(GlobalData.mylatch[j]);
        // Copy the instruction
        output.setInstruction(ins);
        // Send the latch data to the next stage
        output.write();
        
        // And don't forget to indicate that the input was consumed!
  //  instruct=instruct+"[selected]";
      // setActivity(instruct);
     GlobalData.Queue[j]="NULL";
     getCore().incDispatched();
   
  }
		
}
        
        // If and only if there is a LOAD in the FIRST entry of the LSQ, it can be
        // issued to the DCache if needed inputs (to compute the address)
        // will be available net cycle.  
        // Set appropriate "forward#" properties on output latch.
        // Only when a LOAD is not prededed by STOREs can be be issued with
        // forwarding in the next cycle.
        // ******
        // When issuing any LOAD, other entries in the LSQ must be shifted
        // to fill the gap.  The LSQ must maintain program order.  This applies
        // to all cases where the LSQ issues a LOAD.
        // ******
        
        // If we issued a load, bail out.  ** Before bailing out, ALWAYS make sure
        // that setActivity has been called with info about all the 
        // instructons in the queue.  This is for diagnostic purposes. **
                
        // Look for a load whose address matches that of a store earlier in the list.
        // Since we don't do speculative loads, if a store is encountered with an
        // unknown address, then no subsequent loads can be issued.
        
        // Outer loop:  Iterate over all LOAD instructions from first to last
        // LSQ entries.
        // Inner loop:  Iterate backwards over STOREs that came before the LOAD.
        // If you find a STORE with a unknown address, skip to the next LOAD
        // in the outer loop.
        
        // If you find a STORE with a matching address, make sure the STORE
        // has a data value. If it does, this LOAD can be ussued as a BYPASS LOAD:
        // copy the value from the STORE to the LOAD and issue the load, 
        // instructing the DCache to NOT fetch from memory.
        // If the STORE does not have a data value, skip to the next LOAD in 
        // the outer loop.  
        // Data that is forwarded from a STORE to a LOAD must some from the
        // matching STORE that is NEAREST to the LOAD in the list.
        
        // If the inner loop finishes and finds neither a matching STORE addresss
        // nor an unknown store address, this LOAD can be issued as an ACCESS
        // LOAD:  The data is fetched from main memory.
                
        // If we issued a LOAD, set activity string, bail out

        // If we find no LOADs to process, see if there are any STORES to ISSUE.
        // An issuable store has known address and data.  To the DCache,
        // an ISSUE STORE passes through to Writeback without modifying memory.  (It will
        // modify memory later on retirement.)  Also, stores that are issued
        // are NOT REMOVED from the LSQ.  Simply mark them as completed.
        
        // If we issued a STORE, set activity string, bail out
        
        // Finally, see if there is a STORE that can be COMMITTED (retired).
        // Only the FIRST entry of the LSQ can be retired (to maintain 
        // program order).
        // To the DCache, a COMMIT STORE writes its data value to memory
        // but is NOT passed on to Writeback.
        
        // Set activity string, return        
        
        // NOTE:
        // ***
        // Whenever you issue any instruction, be sure to call core.incIssued();
        // This is also the case for the IssueQueue.
        // ***
    }
}
