package implementation;
import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import utilitytypes.ICpuCore;
import utilitytypes.IGlobals;

/*** FloatDiv Stage ***/
    
    public class IntDiv extends PipelineStageBase {
        public IntDiv(ICpuCore core) {
            super(core, "IntDiv");
        }

        @Override
        public void compute(Latch input, Latch output) {
        	  IGlobals globals = (GlobalData)getCore().getGlobals();
        	  if (input.isNull()) return;
            doPostedForwarding(input);
            InstructionBase ins = input.getInstruction();
            
            int source1 = ins.getSrc1().getValue();
            int source2 = ins.getSrc2().getValue();
         
            	int result = source1 / source2;
            	
            	boolean isfloat = ins.getSrc1().isFloat() || ins.getSrc2().isFloat();
                if(GlobalData.IntDiv<15) {
                	GlobalData.IntDiv++;
                	setResourceWait("Loop "+GlobalData.IntDiv);
                }
                else {
            	GlobalData.IntDiv=0;
                }
                output.setResultValue(result);
                output.setInstruction(ins);
                       
        }
    }