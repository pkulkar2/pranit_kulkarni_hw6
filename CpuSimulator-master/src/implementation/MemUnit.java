package implementation;

import static utilitytypes.IProperties.MAIN_MEMORY;

import baseclasses.FunctionalUnitBase;
import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import utilitytypes.EnumOpcode;
//import implementation.MultiStageFunctionalUnit.MyMathUnit;
import utilitytypes.ICpuCore;
import utilitytypes.IGlobals;
import utilitytypes.IModule;
import utilitytypes.Operand;

public class MemUnit extends FunctionalUnitBase {
    public MemUnit(IModule parent, String name) {
        super(parent, name);
    }
    public static class Addr extends PipelineStageBase {
        public Addr(IModule parent) {
            // For simplicity, we just call this stage "in".
            super(parent, "in:Addr");
//            super(parent, "in:Math");  // this would be fine too
        }
        
        @Override
        public void compute(Latch input, Latch output) {
            if (input.isNull()) return;
            doPostedForwarding(input);
            InstructionBase ins = input.getInstruction();
          //  setActivity(ins.toString());
            int source1 = ins.getSrc1().getValue();
            int source2 = ins.getSrc2().getValue();

            int addr = source1 + source2;
                   output.setProperty("address",addr);
            //output.setResultValue(addr);
            output.setInstruction(ins);
        }
    }
    
    public static class LSQ extends PipelineStageBase {
        public LSQ(IModule parent) {
            // For simplicity, we just call this stage "in".
            super(parent, "LSQ");
//            super(parent, "in:Math");  // this would be fine too
        }
        
        @Override
        public void compute(Latch input, Latch output) {
            if (input.isNull()) return;
            doPostedForwarding(input);
            InstructionBase ins = input.getInstruction();
            setActivity(ins.toString());
       //     int source1 = ins.getSrc1().getValue();
         //   int source2 = ins.getSrc2().getValue();
//
            int addr2=input.getPropertyInteger("address");
           // int result = input.getResultValue();
            addStatusWord("Addr="  + input.getPropertyInteger("address"));
            output.setProperty("address",addr2);
         //   output.setResultValue(result);
            output.setInstruction(ins);
        }
    }
    
    public static class DCache extends PipelineStageBase {
        public DCache(IModule parent) {
            // For simplicity, we just call this stage "in".
            super(parent, "DCache");
//            super(parent, "in:Math");  // this would be fine too
        }
        
        @Override
        public void compute(Latch input, Latch output) {
            if (input.isNull()) return;
            doPostedForwarding(input);
            InstructionBase ins = input.getInstruction();

            Operand oper0 = ins.getOper0();
            int oper0val = ins.getOper0().getValue();
            
            setActivity(ins.toString());
            // The Memory stage no longer follows Execute.  It is an independent
            // functional unit parallel to Execute.  Therefore we must perform
            // address calculation here.
            int addr = input.getPropertyInteger("address");
            
            int value = 0;
            IGlobals globals = (GlobalData)getCore().getGlobals();
            int[] memory = globals.getPropertyIntArray(MAIN_MEMORY);

            switch (ins.getOpcode()) {
                case LOAD:
                    // Fetch the value from main memory at the address
                    // retrieved above.
                    value = memory[addr];
                   output.setResultValue(value);
                    output.setInstruction(ins);
                    addStatusWord(ins.getOper0().getRegisterName()+"=Mem[" + addr + "]");
                    break;
                
                case STORE:
                    // For store, the value to be stored in main memory is
                    // in oper0, which was fetched in Decode.
                    memory[addr] = oper0val;
                    addStatusWord("Mem[" + addr + "]=" + ins.getOper0().getValueAsString());
            
                    output.setInstruction(ins);
                    return;
                    
                default:
                    throw new RuntimeException("Non-memory instruction got into Memory stage");
            }
        }
    }
    
	@Override
	public void createPipelineRegisters() {
		// TODO Auto-generated method stub
		createPipeReg("AddrToLSQ");
		createPipeReg("LSQToDCache");
		createPipeReg("DCachetoWriteback");
	}

	@Override
	public void createPipelineStages() {
		// TODO Auto-generated method stub
		addPipeStage(new MemUnit.Addr(this));
		addPipeStage(new MemUnit.LSQ(this));
		addPipeStage(new MemUnit.DCache(this));
	}

	@Override
	public void createChildModules() {
		// TODO Auto-generated method stub
		/*new Addr(this);
		new LSQ(this);
		new DCache(this);*/
	}

	@Override
	public void createConnections() {
		// TODO Auto-generated method stub
		 connect("in:Addr", "AddrToLSQ", "LSQ");
		 connect("LSQ","LSQToDCache","DCache");
		 addRegAlias("DCachetoWriteback", "out");
		 connect("DCache","DCachetoWriteback");
	       
		 
		 
	
		 //connect("DCache","");
		 
		 
	}

	@Override
	public void specifyForwardingSources() {
		// TODO Auto-generated method stub
		 addForwardingSource("out");
		
	}
}

