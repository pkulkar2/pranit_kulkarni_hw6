package implementation;
import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import utilitytypes.ICpuCore;
import utilitytypes.IGlobals;

/*** FloatDiv Stage ***/
    
    public class FloatDiv extends PipelineStageBase {
        public FloatDiv(ICpuCore core) {
            super(core, "FloatDiv");
        }

        @Override
        public void compute(Latch input, Latch output) {
        	  IGlobals globals = (GlobalData)getCore().getGlobals();
        	  if (input.isNull()) return;
            doPostedForwarding(input);
            InstructionBase ins = input.getInstruction();
            
            float source1 = ins.getSrc1().getFloatValue();
            float source2 = ins.getSrc2().getFloatValue();
         
            	float result = source1 / source2;
            	
            	boolean isfloat = ins.getSrc1().isFloat() || ins.getSrc2().isFloat();
                if(GlobalData.FloatDiv<15) {
                	GlobalData.FloatDiv++;
                	setResourceWait("Loop "+GlobalData.FloatDiv);
                }
                else {
            	GlobalData.FloatDiv=0;
                }
                output.setResultFloatValue(result);
                output.setInstruction(ins);
                       
        }
    }