/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.InstructionBase;
import baseclasses.InstructionBase.EnumBranch;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import utilitytypes.EnumComparison;
import utilitytypes.EnumOpcode;
import utilitytypes.IModule;
import utilitytypes.IProperties;

/**
 *
 * @author millerti
 */
public class BranchResUnit extends PipelineStageBase {

    public BranchResUnit(IModule parent) {
        super(parent, "BranchResUnit");
    }
    
    static boolean resolveBranch(EnumComparison condition, int value0) {
        // Add code here...
    	return true;
    }

    @Override
    public void compute(Latch input, Latch output) {
        if (input.isNull()) return;
        doPostedForwarding(input);
        InstructionBase ins = input.getInstruction().duplicate();

        /*
        JMP -- pass through
        BRA -- resolve, compare to prediction, set fault on instruction for disagreement
        CALL -- compute return address and pass on as result value.
        */
        EnumOpcode opcode = ins.getOpcode();
          switch(opcode) {
          case JMP:
        	  output.setInstruction(ins); 
        	  return;
          case CALL:
        	  int result=ins.getSrc1().getValue()+ins.getSrc2().getValue();
        	  output.setResultValue(result);
        	  output.setInstruction(ins);
        	  return;
          case BRA:
        	 int value0 = ins.getOper0().getValue();
        	 boolean take_branch=false;
              // The CMP instruction just sets its destination to
              // (src1-src2).  The result of that is in oper0 for the
              // BRA instruction.  See comment in MyALU.java.
              switch (ins.getComparison()) {
                  case EQ:
                      take_branch = (value0 == 0);
                      break;
                  case NE:
                      take_branch = (value0 != 0);
                      break;
                  case GT:
                      take_branch = (value0 > 0);
                      break;
                  case GE:
                      take_branch = (value0 >= 0);
                      break;
                  case LT:
                      take_branch = (value0 < 0);
                      break;
                  case LE:
                      take_branch = (value0 <= 0);
                      break;
              }
               EnumBranch predicted_branch = ins.getBranchPrediction();
               EnumBranch resolve_branch = ins.getBranchResolution();
               if(predicted_branch==resolve_branch) {
            	   ins.setFault(InstructionBase.EnumFault.BRANCH);
               }
               
              
          }
               
    }
    
    
}
