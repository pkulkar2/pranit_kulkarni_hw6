/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.PipelineRegister;
import baseclasses.PipelineStageBase;
import baseclasses.CpuCore;
import examples.MultiStageFunctionalUnit;
import java.util.Set;
import tools.InstructionSequence;
import utilitytypes.ClockedIntArray;
import utilitytypes.IClocked;
import utilitytypes.IGlobals;
import utilitytypes.IPipeReg;
import utilitytypes.IPipeStage;
import utilitytypes.IProperties;
import static utilitytypes.IProperties.*;
import utilitytypes.IRegFile;
import utilitytypes.Logger;
import voidtypes.VoidRegister;

/**
 * This is an example of a class that builds a specific CPU simulator out of
 * pipeline stages and pipeline registers.
 * 
 * @author 
 */
public class MyCpuCore extends CpuCore {
    static final String[] producer_props = {RESULT_VALUE};
        
    /**
     * Method that initializes the CpuCore.
     */
    @Override
    public void initProperties() {
        // Instantiate the CPU core's property container that we call "Globals".
        properties = new GlobalData();
       IRegFile arf=((IGlobals)properties).getPropertyRegisterFile(ARCH_REG_FILE);
      // int[] arcf=((IGlobals)properties).getPropertyIntArray(GlobalData.ARF_SIZE);	
// Set all RAT entries to -1, mapping architectural register numbers to the ARF.
// Set all ARF entries as USED and VALID
       
   for(int i=0;i<32;i++) {
       GlobalData.RAT[i]=-1;
    arf.changeFlags(i, 4, 1 );
   
        	//System.out.println(RAT[i]);
        }
        for(int i=0;i<256;i++) {
            GlobalData.Queue[i]="NULL";
             	//System.out.println(RAT[i]);
             }
      
    }
    
    public void loadProgram(InstructionSequence program) {
        getGlobals().loadProgram(program);
    }
    
    public void runProgram() {
        properties.setProperty(IProperties.CPU_RUN_STATE, IProperties.RUN_STATE_RUNNING);
        while (properties.getPropertyInteger(IProperties.CPU_RUN_STATE) != IProperties.RUN_STATE_HALTED) {
            Logger.out.println("## Cycle number: " + cycle_number);
            Logger.out.println("# State: " + getGlobals().getPropertyInteger(IProperties.CPU_RUN_STATE));
 for(int i=0;i<256;i++)
           {
        	   if(!getGlobals().getRegisterFile().isInvalid(i) && getGlobals().getRegisterFile().isRenamed(i) && getGlobals().getRegisterFile().isUsed(i) )
        	   {
        		   getGlobals().getRegisterFile().markUsed(i,false);
        		  // regfile.changeFlags(number,IRegFile.SET_USED | IRegFile.SET_INVALID, IRegFile.CLEAR_FLOAT | IRegFile.CLEAR_RENAMED);
        		   Logger.out.println("#Freeing P"+i);
        	   }
           }
            IClocked.advanceClockAll();
        }
    }

    @Override
    public void createPipelineRegisters() {
        createPipeReg("FetchToDecode");
        createPipeReg("DecodeToIssueQueue");
        createPipeReg("DecodeToLSQ");
        createPipeReg("IssueQueueToExecute");
        createPipeReg("IssueQueueToIntMul");
        createPipeReg("IssueQueueToFloatMul");
        createPipeReg("IssueQueueToIntDiv");
      //  createPipeReg("IssueQueueToMemUnit");
        createPipeReg("IssueQueueToFloatDiv");
        createPipeReg("IssueQueueToBranchResUnit");
        createPipeReg("IssueQueueToFloatAddSub");
        createPipeReg("IntDivToWriteback");
        createPipeReg("FloatDivToWriteback");
        createPipeReg("ExecuteToWriteback");
        createPipeReg("BranchResUnitToWriteback");
    }

    @Override
    public void createPipelineStages() {
        addPipeStage(new AllMyStages.Fetch(this));
        addPipeStage(new AllMyStages.Decode(this));
        addPipeStage(new AllMyStages.IssueQueue(this));
        addPipeStage(new AllMyStages.Execute(this));
        addPipeStage(new IntDiv(this));
        addPipeStage(new BranchResUnit(this));
        addPipeStage(new FloatDiv(this));
        addPipeStage(new Writeback(this));
        addPipeStage(new Retirement(this));
    }

    @Override
    public void createChildModules() {
        // MSFU is an example multistage functional unit.  Use this as a
        // basis for FMul, IMul, and FAddSub functional units.
        addChildUnit(new IntMul(this, "IntMul"));
        addChildUnit(new FMul(this, "FloatMul"));
        addChildUnit(new MemUnit(this, "MemUnit"));
        addChildUnit(new FAddSub(this, "FloatAddSub"));
    }

    @Override
    public void createConnections() {
        // Connect pipeline elements by name.  Notice that 
        // Decode has multiple outputs, able to send to Memory, Execute,
        // or any other compute stages or functional units.
        // Writeback also has multiple inputs, able to receive from 
        // any of the compute units.
        // NOTE: Memory no longer connects to Execute.  It is now a fully 
        // independent functional unit, parallel to Execute.
        
        // Connect two stages through a pipelin register
        connect("Fetch", "FetchToDecode", "Decode");
        
        // Decode has multiple output registers, connecting to different
        // execute units.  
        // "MSFU" is an example multistage functional unit.  Those that
        // follow the convention of having a single input stage and single
        // output register can be connected simply my naming the functional
        // unit.  The input to MSFU is really called "MSFU.in".
        connect("Decode", "DecodeToIssueQueue", "IssueQueue");
        connect("Decode", "DecodeToLSQ", "MemUnit");
        connect("IssueQueue", "IssueQueueToExecute", "Execute");
       // connect("Decode", "DecodeToMemory", "Memory");
        connect("IssueQueue", "IssueQueueToIntMul", "IntMul");
        connect("IssueQueue", "IssueQueueToFloatMul", "FloatMul");
        connect("IssueQueue", "IssueQueueToIntDiv", "IntDiv");
        connect("IssueQueue", "IssueQueueToFloatDiv", "FloatDiv");
        connect("IssueQueue", "IssueQueueToFloatAddSub", "FloatAddSub");
      
       connect("IssueQueue", "IssueQueueToBranchResUnit", "BranchResUnit");
       connect("BranchResUnit","BranchResUnitToWriteback","Writeback");
        // Writeback has multiple input connections from different execute
        // units.  The output from MSFU is really called "MSFU.Delay.out",
        // which was aliased to "MSFU.out" so that it would be automatically
        // identified as an output from MSFU.
        connect("IntDiv","IntDivToWriteback", "Writeback");
        connect("FloatDiv","FloatDivToWriteback", "Writeback");
        connect("Execute","ExecuteToWriteback", "Writeback");
       // connect("Memory", "MemoryToWriteback", "Writeback");
        connect("IntMul", "Writeback");
        connect("FloatMul", "Writeback");  
        connect("FloatAddSub", "Writeback");
        connect("MemUnit","Writeback");
       
    }

    @Override
    public void specifyForwardingSources() {
        addForwardingSource("ExecuteToWriteback");
        addForwardingSource("FloatDivToWriteback");
        addForwardingSource("IntDivToWriteback");
        
        // MSFU.specifyForwardingSources is where this forwarding source is added
        // addForwardingSource("MSFU.out");
    }

    @Override
    public void specifyForwardingTargets() {
        // Not really used for anything yet
    }

    @Override
    public IPipeStage getFirstStage() {
        // CpuCore will sort stages into an optimal ordering.  This provides
        // the starting point.
        return getPipeStage("Fetch");
    }
    
    public MyCpuCore() {
        super(null, "core");
        initModule();
        printHierarchy();
        Logger.out.println("");
    }
}
